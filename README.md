# YouLess Home Assistant Component

This component adds support for the [Youless](http://www.youless.nl) energy meter reader to Home Assistant. The component will
create a sensor for the current power consumption measured.

**Note:** Support for the LS110 is experimental currently and is registered in issue [YA-1](https://jongsoftdev.atlassian.net/browse/YA-1).

## Installation instructions

* Download one of the releases of this component
* Upload the component in the following directory on home assistant

```
/config
```

## Setup

Go to the integrations page in your configuration and click on new integration -> YouLess. 

You will need the following two things when creating the integration:

* a name, this is used in the naming of entities
* the host, this should be the IP address of the device

The integration will automatically create a device and various sensors in your Home Assistant. The integration will attempt to
determine wether you have a LS110 or a LS120 device.

## Configuration example

Configuration through the configuration.yaml is no longer possible.

## Exposed sensors

Which sensors you get depends on which device you have. The integration should create one device with the following sensors:

* Current power consumption
* Total power consumption

And additionally for the LS120 only:

* Total gas consumption
* Power consumption low
* Power consumption high
* Power delivery total
* Power delivery current