# YouLess Home Assistant Component

This component adds support for the [Youless](http://www.youless.nl) energy meter reader to Home Assistant. The component will
create a sensor for the current power consumption measured.

## Installation instructions

* Download one of the releases of this component
* Upload the component in the following directory on home assistant

```
/config
```

## Setup

Go to the integrations page in your configuration and click on new integration -> YouLess. 
You will need to know the IP address that your YouLess device has in your local network. 

The integration will automatically create a device and various sensors in your installation.

## Configuration example

Configuration through the configuration.yaml is no longer possible.